package com.example.truthtableapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.truthtableapp.databinding.ActivityTruthTableBinding

class TruthTableActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTruthTableBinding

    private var status: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityTruthTableBinding.inflate(layoutInflater)

        with(binding) {
            setContentView(root)
            submitButton.setOnClickListener {
                checkAnswer()
            }

        }

    }

    private fun checkAnswer() {
        status = 0
        with(binding) {
            if ((trueTrue.text.toString().toUpperCase() != "F") ||
                (trueFalse.text.toString().toUpperCase() != "T") ||
                (falseTrue.text.toString().toUpperCase() != "F") ||
                (falseFalse.text.toString().toUpperCase() != "F")
            ) {
                onAnswerIncorrect()
            } else {
                onAnswerCorrect()
            }
        }


    }

    /**
     * Displays a successful Toast message.
     */
    private fun onAnswerCorrect() {
        Toast.makeText(this, R.string.correct, Toast.LENGTH_SHORT).show()
    }

    /**
     * Displays a incorrect Toast message.
     */
    private fun onAnswerIncorrect() {
        Toast.makeText(this, R.string.incorrect, Toast.LENGTH_SHORT).show()
    }
}
