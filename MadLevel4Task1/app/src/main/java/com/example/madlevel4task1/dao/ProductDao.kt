package com.example.madlevel4task1.dao

import androidx.room.*
import com.example.madlevel4task1.model.Product


/**
 * Created by R. Dekker on 23-6-2021.
 */
@Dao
interface ProductDao {

    @Query("SELECT * FROM productTable")
    suspend fun getAllProducts(): List<Product>


    suspend fun insertProduct(product: Product)

    @Delete
    suspend fun deleteProduct(product: Product)

    @Query("DELETE FROM productTable")
    suspend fun deleteAllProducts()

}