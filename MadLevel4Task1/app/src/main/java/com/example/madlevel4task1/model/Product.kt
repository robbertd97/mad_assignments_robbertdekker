package com.example.madlevel4task1.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize


/**
 * Created by R. Dekker on 23-6-2021.
 */
@Parcelize
@Entity(tableName = "productTable")
data class Product(
    @ColumnInfo(name = "product")
    var productName: String,
    @ColumnInfo(name = "quantity")
    var productQuantity: Int,
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long? = null
) : Parcelable