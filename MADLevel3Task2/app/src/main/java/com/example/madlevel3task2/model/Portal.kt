package com.example.madlevel3task2.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Created by R. Dekker on 18-6-2021.
 */
@Parcelize
data class Portal (
    val title: String,
    val url: String
): Parcelable