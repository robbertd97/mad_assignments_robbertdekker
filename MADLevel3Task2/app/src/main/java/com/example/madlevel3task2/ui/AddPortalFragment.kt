package com.example.madlevel3task2.ui

import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import com.example.madlevel3task2.model.Portal
import com.example.madlevel3task2.ui.StudentPortalFragment.Companion.ARG_ADD_PORTAL
import com.example.madlevel3task2.ui.StudentPortalFragment.Companion.ARG_PORTAL
import com.example.madlevel3task2.databinding.FragmentAddPortalBinding

/**
 * Created by R. Dekker on 18-6-2021.
 */
class AddPortalFragment : Fragment() {

    private var _binding: FragmentAddPortalBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddPortalBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initViews() {
        binding.btnAdd.setOnClickListener {
            onSaveClick()
        }
    }

    private fun onSaveClick() {
        if (binding.etTitle.text.toString().isNotBlank()) {
            val portal = Portal(
                binding.etTitle.text.toString(),
                binding.etUrl.text.toString()
            )

            setFragmentResult(
                ARG_ADD_PORTAL, bundleOf(
                    ARG_PORTAL to portal
                )
            )
            findNavController().popBackStack()

        } else {
            Toast.makeText(activity,"The Title cannot be empty"
                , Toast.LENGTH_SHORT).show()
        }
    }
}