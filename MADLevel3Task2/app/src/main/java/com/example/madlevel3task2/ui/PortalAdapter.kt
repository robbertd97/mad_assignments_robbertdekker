package com.example.madlevel3task2.ui


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.madlevel3task2.model.Portal
import com.example.madlevel3task2.R
import com.example.madlevel3task2.databinding.ItemPortalBinding

/**
 * Created by R. Dekker on 18-6-2021.
 */
class PortalAdapter(private val portals: List<Portal>, private val clickListener: (Portal) -> Unit) : RecyclerView.Adapter<PortalAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding = ItemPortalBinding.bind(itemView)

        fun bind(portal: Portal, clickListener: (Portal) -> Unit) {
            binding.tvTitle.text = portal.title
            binding.tvUrl.text = portal.url
            itemView.setOnClickListener { clickListener(portal) }
        }
    }

    /**
     * Creates and returns a ViewHolder object, inflating the layout called item_reminder.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_portal, parent, false)
        )
    }

    /**
     * Returns the size of the list
     */
    override fun getItemCount(): Int {
        return portals.size
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(portals[position], clickListener)
    }
}

