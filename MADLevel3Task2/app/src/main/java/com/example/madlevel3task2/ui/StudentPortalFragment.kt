package com.example.madlevel3task2.ui

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.browser.customtabs.CustomTabsIntent
import androidx.browser.customtabs.CustomTabsSession
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.madlevel3task2.model.Portal
import com.example.madlevel3task2.R
import com.example.madlevel3task2.databinding.FragmentPortalBinding

/**
 * Created by R. Dekker on 18-6-2021.
 */
class StudentPortalFragment : Fragment() {

    private var _binding: FragmentPortalBinding? = null

    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    private var mCustomTabsSession: CustomTabsSession? = null

    private val portals = arrayListOf<Portal>()
    private val portalAdapter = PortalAdapter(portals) { portalItem: Portal ->
        loadCustomTabForSite(portalItem.url)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setFragmentResultListener(
            ARG_ADD_PORTAL
        ) { _, result ->
            result.getParcelable<Portal>(ARG_PORTAL)?.let { note ->
                portals.add(note)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPortalBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()

        binding.fab.setOnClickListener {
            navigateToAddPortalFragment()
        }
    }

    private fun navigateToAddPortalFragment() {
        findNavController().navigate(R.id.action_PortalOverviewFragment_to_AddPortalFragment)
    }

    private fun initViews() {
        // Initialize the recycler view with a linear layout manager, adapter
        binding.rvPortals.layoutManager = GridLayoutManager(
            activity,
            2
        )
        binding.rvPortals.adapter = portalAdapter
    }

    private fun loadCustomTabForSite(url: String, color: Int = Color.BLUE) {
        val customTabsIntent = CustomTabsIntent.Builder(mCustomTabsSession)
            .setToolbarColor(color)
            .setShowTitle(true)
            .build()
        context?.let { customTabsIntent.launchUrl(it, Uri.parse(url)) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val ARG_ADD_PORTAL = "arg_portal_extra"
        const val ARG_PORTAL = "portal"
    }
}