package com.example.notepad.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.notepad.model.Note

@Dao
interface NoteDao {

    @Insert
    suspend fun insertNote(note: Note)

    @Query("SELECT * FROM note_table LIMIT 1")
    fun getNotepad(): LiveData<Note?>

    @Update
    suspend fun updateNote(note: Note)
}