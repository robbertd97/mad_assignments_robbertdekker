package com.example.popularmovieskotlintask2.ui


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.popularmovieskotlintask2.R
import com.example.popularmovieskotlintask2.databinding.ItemMovieBinding
import com.example.popularmovieskotlintask2.model.Movie

/**
 * Created by R. Dekker on 16-08-2021.
 */
class MoviesAdapter(private val movies: List<Movie>, private val onClick: (Movie) -> Unit) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    private lateinit var context: Context
    private lateinit var binding: ItemMovieBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context

        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_movie, parent, false)
        )
    }

    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(movies[position])

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener { onClick(movies[adapterPosition]) }
        }

        private val binding = ItemMovieBinding.bind(itemView)

        fun bind(movieItem: Movie) {
            println(movieItem.poster_path)
            binding.tvNumber.text = ((adapterPosition + 1).toString())
            Glide.with(context).load(movieItem.getPosterUrl()).into(binding.ivMovie)
        }
    }
}
