package com.example.popularmovieskotlintask2.repository

import com.example.popularmovieskotlintask2.api.MoviesApi


/**
 * Created by R. Dekker on 16-08-2021.
 */
class MovieRepository {
    private val movieApi = MoviesApi.createApi()

    fun getMovies(year: String) = movieApi.getMovies(year)
}