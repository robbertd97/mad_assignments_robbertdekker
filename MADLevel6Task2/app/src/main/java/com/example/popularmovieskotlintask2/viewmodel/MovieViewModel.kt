package com.example.popularmovieskotlintask2.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.popularmovieskotlintask2.model.Movie
import com.example.popularmovieskotlintask2.model.MovieResults
import com.example.popularmovieskotlintask2.repository.MovieRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MovieViewModel(application: Application) : AndroidViewModel(application) {

    val error = MutableLiveData<String>()
    val movieList = MutableLiveData<List<Movie>>()
    private val movieRepo = MovieRepository()

    private val mutableSelectedItem = MutableLiveData<Movie>()
    val selectedItem: LiveData<Movie> get() = mutableSelectedItem

    fun selectItem(item: Movie) {
        mutableSelectedItem.value = item
    }

    fun getMovies(year: String) {
        movieRepo.getMovies(year).enqueue(object : Callback<MovieResults> {
            override fun onResponse(call: Call<MovieResults>, response: Response<MovieResults>) {
                if (response.isSuccessful) movieList.value = response.body()?.movies
                else error.value = "An error occurred: ${response.errorBody().toString()}"
            }

            override fun onFailure(call: Call<MovieResults>, t: Throwable) {
                error.value = t.message
            }
        })
    }
}