package com.example.popularmovieskotlintask2.Service

import com.example.popularmovieskotlintask2.model.MovieResults
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Retrieved from "https://developers.themoviedb.org/3/discover/movie-discover" on 16-08-2021.
 */
interface MoviesService {

    @GET("movie")
    fun getMovies(
        @Query("year") year: String,
            @Query("api_key") api_key: String = "3bd7c8c4a8c4ddc66c0d7cf5542851d4",
            @Query("language") language: String = "en",
            @Query("sort_by") sort_by: String = "popularity.desc"
    ): Call<MovieResults>

}