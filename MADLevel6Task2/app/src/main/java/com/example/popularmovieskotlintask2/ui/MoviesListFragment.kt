package com.example.popularmovieskotlintask2.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.popularmovieskotlintask2.R
import com.example.popularmovieskotlintask2.databinding.FragmentMoviesListBinding
import com.example.popularmovieskotlintask2.model.Movie
import com.example.popularmovieskotlintask2.viewmodel.MovieViewModel
import com.google.android.material.snackbar.Snackbar

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 * * Created by R. Dekker on 16-08-2021.
 */
class MoviesListFragment : Fragment() {

    private var _binding: FragmentMoviesListBinding? = null
    private val binding get() = _binding!!

    private val movies = arrayListOf<Movie>()
    private lateinit var moviesAdapter: MoviesAdapter

    private val viewModel: MovieViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMoviesListBinding.inflate(inflater, container, false)
        return binding.root
    }

    /**
     * Calls the initViews and observeMovies methods
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        observeMovies()
    }


    /**
     * Calls the initViews and observeMovies methods
     */
    private fun initViews() {
        moviesAdapter = MoviesAdapter(movies, ::onMovieClick)
        binding.rvMovies.layoutManager =
            GridLayoutManager(activity, 2, RecyclerView.VERTICAL, false)
        binding.rvMovies.adapter = moviesAdapter

        // Binds the Submit button to the 'getMovies'-action
        // while extracting the specified year from the txtYear-field.
        binding.btnSubmit.setOnClickListener {
            viewModel.getMovies(binding.txtYear.text.toString())
        }
    }

    /**
     * Clears all the movies
     */
    private fun observeMovies() {
        viewModel.movieList.observe(viewLifecycleOwner, androidx.lifecycle.Observer { movies ->
            this@MoviesListFragment.movies.clear()
            this@MoviesListFragment.movies.addAll(movies)
            moviesAdapter.notifyDataSetChanged()
        })
    }

    private fun onMovieClick(movie: Movie) {
        viewModel.selectItem(movie)
        findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)

        Snackbar.make(binding.rvMovies, "Movie: ${movie.title}", Snackbar.LENGTH_LONG).show()
    }
}