package com.example.popularmovieskotlintask2.model

import com.google.gson.annotations.SerializedName

data class MovieResults(@SerializedName("results") var movies: List<Movie>)