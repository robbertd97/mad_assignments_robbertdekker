package com.example.popularmovieskotlintask2.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.example.popularmovieskotlintask2.databinding.FragmentMovieDetailBinding
import com.example.popularmovieskotlintask2.model.Movie
import com.example.popularmovieskotlintask2.viewmodel.MovieViewModel

/**
 * Created by R. Dekker on 16-08-2021.
 */
class MovieDetailFragment : Fragment() {

    private var _binding: FragmentMovieDetailBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MovieViewModel by activityViewModels()
    private val movies = arrayListOf<Movie>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMovieDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    /**
     * Clears all the movies before adding the new movies of the selected year
     */
    private fun observeMovies() {
        viewModel.selectedItem.observe(viewLifecycleOwner, androidx.lifecycle.Observer { movies ->
            this@MovieDetailFragment.movies.clear()
            this@MovieDetailFragment.movies.add(movies)
        })
    }

    /**
     * Binds all the retrieved information to the corresponding UI elements
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeMovies()

        binding.tvTitle.text = viewModel.selectedItem.value?.title
        binding.tvStar.text = viewModel.selectedItem.value?.vote_average.toString()
        binding.tvOverview.text = viewModel.selectedItem.value?.overview
        binding.tvDate.text = viewModel.selectedItem.value?.release_date
        Glide.with(this).load(viewModel.selectedItem.value?.getPosterUrl()).into(binding.ivPoster)
        Glide.with(this).load(viewModel.selectedItem.value?.getBackdropUrl())
            .into(binding.ivBackdrop)
    }
}