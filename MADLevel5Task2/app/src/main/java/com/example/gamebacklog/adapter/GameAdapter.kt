package com.example.gamebacklog.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.gamebacklog.model.Game
import com.example.gamebacklog.ui.GameCardViewHolder

class GameAdapter(private val games: List<Game>) : RecyclerView.Adapter<GameCardViewHolder>() {
    /**
     * Creates and returns a ViewHolder object, inflating the layout called item_reminder.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameCardViewHolder =
        GameCardViewHolder.create(parent)

    /**
     * Called by RecyclerView to display the data at the specified position.
     */
    override fun onBindViewHolder(holder: GameCardViewHolder, position: Int) {
        with(holder) { bind(games[position]) }
    }

    /**
     * Returns the size of the list
     */
    override fun getItemCount(): Int {
        return games.size
    }


}