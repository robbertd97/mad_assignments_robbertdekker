package com.example.gamebacklog.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.gamebacklog.model.Game

/**
 * Created by R. Dekker on 26-7-2021.
 */

@Dao
interface GameDao {

    @Query("SELECT * FROM backlog_table")
    fun getAllGames(): LiveData<List<Game>>

    @Insert
    suspend fun insertGame(game: Game)

    @Delete
    suspend fun deleteGame(game: Game)

    @Query("SELECT * FROM backlog_table LIMIT 1")
    fun getGame(): LiveData<Game?>

    @Update
    suspend fun updateGame(game: Game)

    @Query("DELETE FROM backlog_table")
    suspend fun deleteGames()
}