package com.example.gamebacklog.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity(tableName = "backlog_table")
data class Game(
    @ColumnInfo(name = "title")
    var title: String,

    @ColumnInfo(name = "platform")
    var platform: String,

    @ColumnInfo(name = "date")
    var date: Date,

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int? = null
) : Parcelable, Comparable<Date> {
    override operator fun compareTo(other: Date): Int {
        if (this.date.year > other.year) return 1
        if (this.date.year < other.year) return -1
        if (this.date.month > other.month) return 1
        if (this.date.month < other.month) return -1
        if (this.date.day > other.day) return 1
        if (this.date.day < other.day) return -1
        return 0
    }
}