package com.example.gamebacklog.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.gamebacklog.databinding.GameCardBinding
import com.example.gamebacklog.model.Game
import java.text.SimpleDateFormat


/**
 * Created by R. Dekker on 26-7-2021.
 */
class GameCardViewHolder private constructor(
    private val binding: GameCardBinding
) : RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup): GameCardViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = GameCardBinding.inflate(layoutInflater, parent, false)
            return GameCardViewHolder(
                binding
            )
        }
    }

    fun bind(game: Game) {
        binding.tvTitle.text = game.title
        binding.tvPlatform.text = game.platform

        val monthDate = SimpleDateFormat("MMMM")
        val monthName: String = monthDate.format(game.date.month)

        binding.tvDate.text =
            "Release date: " + game.date.date + " " + monthName + " " + game.date.year
    }
}
