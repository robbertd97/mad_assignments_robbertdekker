package com.example.gamebacklog.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.gamebacklog.databinding.FragmentAddGameBinding
import com.example.gamebacklog.model.Game
import com.example.gamebacklog.vm.GameViewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class AddGameFragment : Fragment() {

    private var _binding: FragmentAddGameBinding? = null
    private val binding get() = _binding!!

    private val viewModel: GameViewModel by viewModels()

    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentAddGameBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.fabs.setOnClickListener {
            onAddReminder()
        }

        observeLog()
    }

    @Throws(ParseException::class)
    fun dateValidator(dateStr: String) {
        val formatter = SimpleDateFormat("MMMM", Locale.getDefault())
        val date = formatter.parse(dateStr)
        println(date)
    }

    private fun onAddReminder() {
        val gameTitle = binding.etTitle.text.toString()
        val gamePlatform = binding.etPlatform.text.toString()

        val gameReleaseDay = binding.etDay.text.toString()
        val gameReleaseMonth = binding.etMonth.text.toString()
        val gameReleaseYear = binding.etYear.text.toString()


        var success = false
        try {
            dateValidator(gameReleaseMonth)
            success = true
        } catch (exception: ParseException) {
            Toast.makeText(activity, "Month does not exist.", Toast.LENGTH_SHORT).show()
        }


        if (success) {

            if (gameReleaseDay == "" || gameReleaseMonth == "" || gameReleaseYear == "") {
                Toast.makeText(activity, "Date is empty", Toast.LENGTH_SHORT).show()
            } else {

                viewModel.insertGame(
                    Game(
                        gameTitle,
                        gamePlatform,
                        Date(
                            gameReleaseYear.toInt(), gameReleaseMonth.toInt(),
                            gameReleaseDay.toInt()
                        )
                    )
                )

            }
        }
    }

    private fun observeLog() {
        viewModel.run {
            error.observe(viewLifecycleOwner) { message ->
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
            }

            success.observe(viewLifecycleOwner) {
                findNavController().popBackStack()
            }
        }
    }

}




