package com.example.gamebacklog.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.gamebacklog.database.GameDao
import com.example.gamebacklog.database.GameRoomDatabase
import com.example.gamebacklog.model.Game


/**
 * Created by R. Dekker on 26-7-2021.
 */
class GameRepository(context: Context) {

    private val gameDao: GameDao

    init {
        val database = GameRoomDatabase.getDatabase(context)
        gameDao = database!!.gameDao()
    }

    fun getGame(): LiveData<Game?> {
        return gameDao.getGame()
    }

    fun getAllGames(): LiveData<List<Game>> {
        return gameDao.getAllGames()
    }

    suspend fun deleteGame(game: Game) {
        gameDao.deleteGame(game)
    }

    suspend fun insertGame(game: Game){
        gameDao.insertGame(game)
    }

    suspend fun deleteGames() {
        gameDao.deleteGames()
    }

}