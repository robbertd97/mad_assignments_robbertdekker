package com.example.gamebacklog.vm

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.gamebacklog.repository.GameRepository
import com.example.gamebacklog.model.Game
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GameViewModel(application: Application) : AndroidViewModel(application) {
    private val mainScope = CoroutineScope(Dispatchers.Main)
    private val gameRepository = GameRepository(application.applicationContext)

    val games: LiveData<List<Game>> = gameRepository.getAllGames()
    val game = gameRepository.getGame()
    val error = MutableLiveData<String>()
    val success = MutableLiveData<Boolean>()

    fun insertGame(game: Game) {
        if (isValidGameBacklog(game)) {
            mainScope.launch {
                withContext(Dispatchers.IO) {
                    gameRepository.insertGame(game)
                }
                success.value = true
            }
        }
    }

    fun deleteAll() {
        mainScope.launch {
            gameRepository.deleteGames()
        }
        success.value = true
    }

    fun deleteGame(position: Int) {
        mainScope.launch {
            games.value?.get(position)?.let { gameRepository.deleteGame(it) }
        }
        success.value = true
    }


    private fun isValidGameBacklog(gameBacklog: Game): Boolean {
        return when {
            gameBacklog.title.isBlank() -> {
                error.value = "Title must not be empty!"
                false
            }
            gameBacklog.platform.isBlank() -> {
                error.value = "Platform must not be empty!"
                false
            }
            gameBacklog.date.toString().isBlank() -> {
                error.value = "Release date must not be empty!"
                false
            }
            else -> true
        }
    }
}