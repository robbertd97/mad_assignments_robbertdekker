package com.example.gamebacklog.ui

//import kotlinx.android.synthetic.main.activity_add.*
//import kotlinx.android.synthetic.main.fragment_backlog.*
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.gamebacklog.R
import com.example.gamebacklog.adapter.GameAdapter
import com.example.gamebacklog.databinding.FragmentBacklogBinding
import com.example.gamebacklog.model.Game
import com.example.gamebacklog.vm.GameViewModel
import com.google.android.material.snackbar.Snackbar


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class BacklogFragment : Fragment() {

    //Jetpack binding
    private var _binding: FragmentBacklogBinding? = null
    private val binding get() = _binding!!

    private val viewModel: GameViewModel by viewModels()

    private val games = arrayListOf<Game>()
    private val gameAdapter = GameAdapter(games)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBacklogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        initViews()
        observeAddGameResult()
    }

    private fun initViews() {
        // Initialize the RecyclerView with a linear layout manager, adapter
        binding.rvBacklog.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.rvBacklog.adapter = gameAdapter
        binding.rvBacklog.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )

        createItemTouchHelper().attachToRecyclerView(binding.rvBacklog)

        binding.fabAddGame.setOnClickListener {
            findNavController().navigate(R.id.action_BacklogFragment_to_addNoteFragment)
        }
    }

    private fun observeAddGameResult() {
        viewModel.games.observe(viewLifecycleOwner, androidx.lifecycle.Observer { games ->
            this@BacklogFragment.games.clear()
            this@BacklogFragment.games.addAll(games.sortedBy { game -> game.date })
            gameAdapter.notifyDataSetChanged()
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_delete_all -> {
                val backlogTemp = arrayListOf<Game>()
                backlogTemp.addAll(games)
                viewModel.deleteAll()
                val snackbar = Snackbar.make(
                    binding.rvBacklog,
                    R.string.delete_all,
                    Snackbar.LENGTH_SHORT
                )
                snackbar.setAction(R.string.undo_string, UndoClearBacklog(backlogTemp))
                snackbar.show()
            }
        }
        return true
    }

    private inner class UndoClearBacklog(val backlogArr: ArrayList<Game>) : View.OnClickListener {
        override fun onClick(v: View) {
            for (game in backlogArr) {
                viewModel.insertGame(game)
            }
        }
    }

    /**
     * Create a touch helper to recognize when a user swipes an item from a recycler view.
     * An ItemTouchHelper enables touch behavior (like swipe and move) on each ViewHolder,
     * and uses callbacks to signal when a user is performing these actions.
     */
    private fun createItemTouchHelper(): ItemTouchHelper {
        // Callback which is used to create the ItemTouch helper. Only enables left swipe.
        // Use ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) to also enable right swipe.
        val callback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            // Enables or Disables the ability to move items up and down.
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            // Callback triggered when a user swiped an item.
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                viewModel.deleteGame(position)
                val backlogTemp = arrayListOf<Game>()
                backlogTemp.addAll(games)

                val snackbar = Snackbar.make(
                    binding.rvBacklog,
                    R.string.delete_game,
                    Snackbar.LENGTH_SHORT
                )
                snackbar.setAction(R.string.undo_string, UndoClearBacklog(backlogTemp))
                snackbar.show()
            }
        }
        return ItemTouchHelper(callback)
    }
}