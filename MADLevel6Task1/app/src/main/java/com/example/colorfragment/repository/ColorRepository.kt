package com.example.colorfragment.repository

import com.example.colorfragment.model.ColorItem

/**
 * Created by R. Dekker on 27-7-2021.
 */
class ColorRepository {
    fun getColorItems(): List<ColorItem> {
        return arrayListOf(
            ColorItem("000000", "Black"),
            ColorItem("FF0000", "Red"),
            ColorItem("0000FF", "Blue"),
            ColorItem("FFFF00", "Yellow"),
            ColorItem("008000", "Green")
        )
    }
}
