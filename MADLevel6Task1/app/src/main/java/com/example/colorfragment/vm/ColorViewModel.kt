package com.example.colorfragment.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.colorfragment.repository.ColorRepository
import com.example.colorfragment.model.ColorItem

class ColorViewModel : ViewModel() {
    private val colorRepository = ColorRepository()

    // uses encapsulation to expose as LiveData
    val colorItems: LiveData<List<ColorItem>>
        get() = _colorItems

    private val _colorItems = MutableLiveData<List<ColorItem>>().apply {
        value = colorRepository.getColorItems()
    }
}
