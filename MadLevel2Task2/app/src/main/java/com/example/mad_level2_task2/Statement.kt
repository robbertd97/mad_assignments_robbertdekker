package com.example.mad_level2_task2

data class Statement(
    var statement: String,
    var answer: Boolean
) {

    companion object {
        val STATEMENT = arrayOf(
            "A val and var are the same.",
            "Mobile App Dev grants 12 ECTS",
            "A unit in Kotlin corresponds to a void in Java",
            "In Kotlin, 'when' replaces the 'switch'-operator in Java"
        )

        val STATEMENT_ANSWER = arrayOf(
            true,
            true,
            false,
            true
        )
    }
}