package com.example.mad_level2_task2

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mad_level2_task2.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity(), StatementAdapter.OnItemClickListener {

    private lateinit var binding: ActivityMainBinding

    private val statement = arrayListOf<Statement>()
    private val statementAdapter = StatementAdapter(statement) { partItem: Statement ->
        partItemClicked(partItem)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViews()

    }

    private fun partItemClicked(partItem: Statement) {
        Toast.makeText(this, "${partItem.answer}", Toast.LENGTH_SHORT).show()
    }

    private fun initViews() {

        // Initialize the recycler view with a linear layout manager, adapter
        LinearLayoutManager(
            this@MainActivity,
            RecyclerView.VERTICAL,
            false
        ).also { binding.rvStatement.layoutManager = it }
        binding.rvStatement.adapter = statementAdapter
        binding.rvStatement.addItemDecoration(
            DividerItemDecoration(
                this@MainActivity,
                DividerItemDecoration.VERTICAL
            )
        )
        createItemTouchHelper().attachToRecyclerView(binding.rvStatement)


        for (i in Statement.STATEMENT.indices) {
            statement.add(Statement(Statement.STATEMENT[i], Statement.STATEMENT_ANSWER[i]))
        }
        statementAdapter.notifyDataSetChanged()

    }

    override fun onItemClick(position: Int) {
        Toast.makeText(this, "${Statement.STATEMENT_ANSWER}", Toast.LENGTH_SHORT).show()
    }

    /**
     * Create a touch helper to recognize when a user swipes an item from a recycler view.
     * An ItemTouchHelper enables touch behavior (like swipe and move) on each ViewHolder,
     * and uses callbacks to signal when a user is performing these actions.
     */
    private fun createItemTouchHelper(): ItemTouchHelper {

        // Callback which is used to create the ItemTouch helper.
        // Use ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) to also enable right swipe.
        val callback = object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

            // Enables or Disables the ability to move items up and down.
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            // Callback triggered when a user swiped an item.
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                val position = viewHolder.adapterPosition
                var answer = statement[position].answer

                statement.removeAt(position)
                statementAdapter.notifyDataSetChanged()

                if (direction == 4 && answer) {
                    Snackbar.make(
                        binding.rvStatement,
                        getString(R.string.correct_leftSwipe),
                        Snackbar.LENGTH_SHORT
                    ).show()

                } else if (direction == 4 && !answer) {
                    Snackbar.make(
                        binding.rvStatement,
                        getString(R.string.incorrect_leftSwipe),
                        Snackbar.LENGTH_SHORT
                    ).show()

                } else if (direction == 8 && answer) {
                    Snackbar.make(
                        binding.rvStatement,
                        getString(R.string.incorrect_rightSwipe),
                        Snackbar.LENGTH_SHORT
                    ).show()
                } else if (direction == 8 && !answer) {
                    Snackbar.make(
                        binding.rvStatement,
                        getString(R.string.correct_rightSwipe),
                        Snackbar.LENGTH_SHORT
                    ).show()
                }


            }
        }
        return ItemTouchHelper(callback)
    }

}