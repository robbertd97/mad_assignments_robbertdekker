package com.example.madlevel4task2.enums

/**
 * Created by R. Dekker on 24-6-2021.
 */
enum class Gesture(val text: String) {
    ROCK("rock"),
    PAPER("paper"),
    SCISSORS("scissors");

    open fun compareMoves(otherMove: String): String { // Tie
        if (this.text == otherMove) return "Tie"
        return when (this) {
            ROCK -> if (otherMove == SCISSORS.text) "Win" else "Lost"
            PAPER -> if (otherMove == ROCK.text) "Win" else "Lost"
            SCISSORS -> if (otherMove == PAPER.text) "Win" else "Lost"
        }
    }
}