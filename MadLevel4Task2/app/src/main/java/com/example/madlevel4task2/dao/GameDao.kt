package com.example.madlevel4task2.dao

import androidx.room.*
import com.example.madlevel4task2.model.Game

/**
 * Created by R. Dekker on 24-6-2021.
 */
@Dao
interface GameDao {

    @Query("SELECT * FROM game_table")
    suspend fun getAllGames(): List<Game>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertGame(product: Game)

    @Delete
    suspend fun deleteGame(product: Game)

    @Query("DELETE FROM game_table")
    suspend fun deleteAllGames()

    @Query("SELECT COUNT( * ) FROM game_table WHERE result == 'Win'")
    suspend fun getWinCount(): Int

    @Query("SELECT COUNT( * ) FROM game_table WHERE result == 'Lost'")
    suspend fun getLostCount(): Int

    @Query("SELECT COUNT( * ) FROM game_table WHERE result == 'Tie'")
    suspend fun getDrawCount(): Int

}