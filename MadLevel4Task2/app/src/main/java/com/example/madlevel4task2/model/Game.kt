package com.example.madlevel4task2.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Created by R. Dekker on 24-6-2021.
 */
@Parcelize
@Entity(tableName = "game_table")
data class Game(
    @ColumnInfo(name = "date")
    var date: Long,
    @ColumnInfo(name = "move_computer")
    var moveComputer: String,
    @ColumnInfo(name = "move_user")
    var moveUser: String,
    @ColumnInfo(name = "result")
    var result: String,
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long? = null
): Parcelable