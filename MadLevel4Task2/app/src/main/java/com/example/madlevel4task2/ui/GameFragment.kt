package com.example.madlevel4task2.ui

import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.madlevel4task2.GameRepository
import com.example.madlevel4task2.R
import com.example.madlevel4task2.databinding.FragmentGameBinding
import com.example.madlevel4task2.enums.Gesture
import com.example.madlevel4task2.model.Game
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * Created by R. Dekker on 24-6-2021.
 */
class GameFragment : Fragment() {

    private var _binding: FragmentGameBinding? = null
    private val binding get() = _binding!!

    private lateinit var gameRepository: GameRepository
    private val mainScope = CoroutineScope(Dispatchers.Main)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentGameBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        initViews()
        gameRepository = GameRepository(requireContext())

    }

    private fun initViews() {
        binding.ivRock.setOnClickListener {
            binding.ivYou.setImageResource(R.drawable.rock)
            addResults(Gesture.ROCK)
        }
        binding.ivPaper.setOnClickListener {
            binding.ivYou.setImageResource(R.drawable.paper)
            addResults(Gesture.PAPER)
        }
        binding.ivScissors.setOnClickListener {
            binding.ivYou.setImageResource(R.drawable.scissors)
            addResults(Gesture.SCISSORS)
        }

        getOverallResults()
    }

    private fun computerChoice(): Gesture {
        return when ((0..2).random()) {
            0 -> {
                binding.ivComputer.setImageResource(R.drawable.rock)
                Gesture.ROCK
            }
            1 -> {
                binding.ivComputer.setImageResource(R.drawable.paper)
                Gesture.PAPER
            }
            else -> {
                binding.ivComputer.setImageResource(R.drawable.scissors)
                Gesture.SCISSORS
            }
        }
    }

    private fun addResults(userGesture: Gesture) {
        val current = java.util.Calendar.getInstance()
        val computerMove = computerChoice().text
        val compareGestures: String = userGesture.compareMoves(computerMove)

        mainScope.launch {
            val game = Game(
                date = current.timeInMillis,
                moveComputer = computerMove,
                moveUser = userGesture.text,
                result = compareGestures
            )

            withContext(Dispatchers.IO) {
                gameRepository.insertGame(game)
            }
        }

        binding.tvMainWin.text = compareGestures
        getOverallResults()
    }

    private fun getOverallResults() {
        mainScope.launch {
            val winCount = withContext(Dispatchers.IO) {
                gameRepository.getWinCount()
            }
            val lostCount = withContext(Dispatchers.IO) {
                gameRepository.getLostCount()
            }
            val drawCount = withContext(Dispatchers.IO) {
                gameRepository.getDrawCount()
            }

            "Wins: $winCount  Draws: $drawCount  Losses: $lostCount".also { binding.tvStats.text = it }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        menu.getItem(0).icon =
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_history_white_24dp)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun navigateToHistoryFragment() {
        findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_button -> {
                navigateToHistoryFragment()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}