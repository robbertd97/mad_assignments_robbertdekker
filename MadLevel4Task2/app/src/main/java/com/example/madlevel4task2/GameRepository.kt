package com.example.madlevel4task2

import android.content.Context
import com.example.madlevel4task2.dao.GameDao
import com.example.madlevel4task2.model.Game

/**
 * Created by R. Dekker on 24-6-2021.
 */
class GameRepository(context: Context) {

    private val gameDao: GameDao

    init {
        val database = GameHistoryDatabase.getDatabase(context)
        gameDao = database!!.gameDao()
    }

    suspend fun getAllGames(): List<Game> = gameDao.getAllGames()

    suspend fun insertGame(game: Game) = gameDao.insertGame(game)

    suspend fun deleteGame(game: Game) = gameDao.deleteGame(game)

    suspend fun deleteAllGames() = gameDao.deleteAllGames()

    suspend fun getWinCount() = gameDao.getWinCount()

    suspend fun getLostCount() = gameDao.getLostCount()

    suspend fun getDrawCount() = gameDao.getDrawCount()

}
