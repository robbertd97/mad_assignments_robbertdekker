package com.example.madlevel4task2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.madlevel4task2.model.Game
import com.example.madlevel4task2.databinding.ItemHistoryBinding
import com.example.madlevel4task2.enums.Gesture
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by R. Dekker on 24-6-2021.
 */
class GameAdapter(private val games: List<Game>) : RecyclerView.Adapter<GameAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ItemHistoryBinding.bind(itemView)
        fun bind(game: Game) {
            binding.tvDate.text = getDate(game.date, "dd/MM/yyyy hh:mm:ss")
            binding.ivComputer.setImageResource(drawableConverter(game.moveComputer))
            binding.ivComputer.setImageResource(drawableConverter(game.moveUser))
            binding.tvWin.text = game.result
        }
    }

    /**
     * Convert gesture move
     */
    private fun drawableConverter(move: String): Int {
        when (move) {
            Gesture.ROCK.text -> return R.drawable.rock
            Gesture.PAPER.text -> return R.drawable.paper
        }
        return R.drawable.scissors
    }

    /**
     * Creates and returns a ViewHolder object, inflating the layout called item_reminder.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
        )
    }

    /**
     * Returns the size of the list
     */
    override fun getItemCount(): Int {
        return games.size
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(games[position])
    }

    /**
     * Create a DateFormatter object for displaying date in specified format.
     */
    fun getDate(
        milliSeconds: Long,
        dateFormat: String?
    ): String? {
        val formatter = SimpleDateFormat(dateFormat)
        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar: Calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }
}