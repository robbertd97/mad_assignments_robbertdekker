package com.example.madlevel4task2

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.madlevel4task2.dao.GameDao
import com.example.madlevel4task2.model.Game

/**
 * Created by R. Dekker on 24-6-2021.
 */
@Database(entities = [Game::class], version = 1, exportSchema = false)
abstract class GameHistoryDatabase : RoomDatabase() {

    abstract fun gameDao(): GameDao

    companion object {
        private const val DATABASE_NAME = "GAME_HISTORY_DATABASE"

        @Volatile
        private var gameHistoryDatabaseInstance: GameHistoryDatabase? = null

        fun getDatabase(context: Context): GameHistoryDatabase? {
            if (gameHistoryDatabaseInstance == null) {
                synchronized(GameHistoryDatabase::class.java) {
                    if (gameHistoryDatabaseInstance == null) {
                        gameHistoryDatabaseInstance =
                            Room.databaseBuilder(
                                context.applicationContext,
                                GameHistoryDatabase::class.java,
                                DATABASE_NAME
                            ).build()
                    }
                }
            }
            return gameHistoryDatabaseInstance
        }
    }

}
